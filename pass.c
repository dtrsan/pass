#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sha256.h"
#include "base64.h"

#define SHA256_DIGEST_SIZE 32

void usage(char *name) {
    fprintf(stderr, "Usage: %s <iterations> <pasphrase>\n", name);
    fprintf(stderr, "  * iterations - number of iterations\n");
    fprintf(stderr, "  * passphrase - your passphrase\n\n");
}

int sha256(const unsigned char *in, unsigned long inlen, unsigned char *out) {
    struct sha256_state state;

    sha256_init(&state);
    if(sha256_process(&state, in, inlen) != 0) {
        return -1;
    }
    return sha256_done(&state, out);
}

int main(int argc, char **argv) {

    if (argc != 3) {
        usage(argv[0]);
        return 1;
    }
    long iterations = atol(argv[1]);
    if (iterations <= 0) {
        usage(argv[0]);
        fprintf(stderr, "<iterations> should be a number but was '%s'.\n", argv[1]);
        return 1;
    }

	unsigned char result[SHA256_DIGEST_SIZE];

	sha256((unsigned char *) argv[2], strlen(argv[2]), result);

    for(int i = 1; i < iterations; i++) {
        unsigned char tmp[SHA256_DIGEST_SIZE];
	    sha256(result, SHA256_DIGEST_SIZE, tmp);
        memcpy(result, tmp, SHA256_DIGEST_SIZE);
    }

    size_t out_len;
    unsigned char *str = base64_encode(result, SHA256_DIGEST_SIZE, &out_len);

    printf("SHA256 HEX:\t");
    for(int i = 0; i < SHA256_DIGEST_SIZE; i += 4) {
		printf("%02x%02x%02x%02x", result[i], result[i+1], result[i+2], result[i+3]);
	}
	printf("\nSHA256 BASE64:\t");
    for(int i = 0; i < out_len; i++) {
		printf("%c", *(str+i));
	}
	printf("\n");
	return 0;
}

