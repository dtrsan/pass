# pass

A simple utility to create a password from a passphrase. The utility hashes a
passphrase perfoming n iterations using SHA256 algorithm.

The utility prints generated password as hex string and as BASE64 encoded
string.

## Usage

```
Usage: pass <iterations> <pasphrase>
  * iterations - number of iterations
  * passphrase - your passphrase

```

## Building instructions

To build `pass` run make
```
make pass
```


