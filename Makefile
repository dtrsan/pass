CFLAGS += -std=c99 -Wall -Werror -O2

%.o: %.c
	$(CC) $< -c -o $@ $(CFLAGS)

pass: sha256.o base64.o
	$(CC) sha256.o base64.o $(CFLAGS) pass.c -o pass

base64.o: base64.h base64.c

sha256.o: sha256.h sha256.c

clean:
	rm -f base64.o
	rm -f sha256.o
	rm -f pass

